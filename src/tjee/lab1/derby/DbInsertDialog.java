package tjee.lab1.derby;

import javax.swing.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DbInsertDialog extends JDialog {
    private JPanel contentPane;
    private JButton confirmBt;
    private JButton exitBt;
    private JTextField input_first_name;
    private JTextField input_last_name;
    private JLabel validationSummary;

    public String first_name;
    public String last_name;
    public boolean isValid = false;

    public DbInsertDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(confirmBt);
        validationSummary.setVisible(true);

        confirmBt.addActionListener(e -> onConfirm());

        exitBt.addActionListener(e -> onExit());

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onExit();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onExit(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private boolean requiredFieldValidation(){
        return first_name.length() != 0 && last_name.length() != 0;
    }

    private boolean uniqueValidation(){
        try{
            return DbManager.checkTheExistenceOf(first_name, last_name);
        }catch (SQLException ex){
            Logger.getLogger(DbInsertDialog.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private boolean correctValidation(){
        Pattern pattern = Pattern.compile("^[A-Z][a-z]{2,}");
        Matcher matcher_first_name = pattern.matcher(first_name);
        Matcher matcher_last_name = pattern.matcher(last_name);
        boolean m1_found, m2_found;
        m1_found = matcher_first_name.find();
        m2_found = matcher_last_name.find();

        return m1_found && m2_found;
    }

    private void onConfirm() {
        this.first_name = input_first_name.getText();
        this.last_name = input_last_name.getText();

        if (requiredFieldValidation())
        {
            if (correctValidation())
            {
                if (uniqueValidation())
                {
                    String ADDED_MESSAGE = "Dodano nowy rekord.";
                    validationSummary.setText(ADDED_MESSAGE);
                    this.isValid = true;
                }
                else {
                    String UNIQUE_VALIDATION_MESSAGE = "Podana osoba już znajduje się w bazie!";
                    validationSummary.setText(UNIQUE_VALIDATION_MESSAGE);
                }
            }
            else{
                String CORRECT_VALIDATION_MESSAGE = "Dane niezgodne ze wzorcem!";
                validationSummary.setText(CORRECT_VALIDATION_MESSAGE);
            }
        }
        else
        {
            String REQUIRED_FIELD_VALIDATION_MESSAGE = "Wymagane wszystkie pola!";
            validationSummary.setText(REQUIRED_FIELD_VALIDATION_MESSAGE);
        }
    }

    private void onExit() {
        dispose();
    }
}
