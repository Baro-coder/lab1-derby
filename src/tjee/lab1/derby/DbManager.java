package tjee.lab1.derby;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Objects;

public class DbManager {
    public static final String DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
    public static final String JDBC_URL = "jdbc:derby:./db/ludzie";
    public static final String QUERY_GET = "select * from app.pracownicy";
    public static final String QUERY_CHECK_TEMPLATE = "select count(*) from app.pracownicy where first_name='";
    public static final String QUERY_SET_TEMPLATE = "insert into app.pracownicy(first_name, last_name) values";
    private static java.sql.Connection conn;
    private DbManager() { }
    public static boolean Connect() throws ClassNotFoundException, SQLException {
        conn = DriverManager.getConnection(JDBC_URL);
        return conn != null;
    }
    public static boolean Disconnect() throws SQLException {
        if (conn == null) {
            return false;
        } else {
            conn.close();
            return true;
        }
    }
    public static String getData() throws SQLException {
        Statement stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(QUERY_GET);
        ResultSetMetaData rsmd = rs.getMetaData();
        String wiersz = "";
        int colCount = rsmd.getColumnCount();
        for (int i = 1; i <= colCount; i++) {
            wiersz = wiersz.concat(rsmd.getColumnName(i) + " \t| ");
        }
        wiersz = wiersz.concat("\r\n");
        while (rs.next()) {
            for (int i = 1; i <= colCount; i++) {
                wiersz = wiersz.concat(rs.getString(i) + " \t| ");
            }
            wiersz = wiersz.concat("\r\n");
        }
        stat.close();
        return wiersz; }
    public static void addRecord(String first_name, String last_name) throws SQLException{
        Statement stat = conn.createStatement();
        String querySet = QUERY_SET_TEMPLATE.concat("('").concat(first_name)
                .concat("','").concat(last_name).concat("')");
        stat.executeUpdate(querySet);
    }

    public static boolean checkTheExistenceOf(String first_name, String last_name) throws SQLException
    {
        Statement stat = conn.createStatement();
        String queryCheck = QUERY_CHECK_TEMPLATE.concat(first_name)
                .concat("' and last_name='").concat(last_name).concat("'");
        ResultSet rs = stat.executeQuery(queryCheck);
        ResultSetMetaData rsmd = rs.getMetaData();

        String wiersz = "";
        int colCount = rsmd.getColumnCount();
        while (rs.next()) {
            for (int i = 1; i <= colCount; i++) {
                wiersz = wiersz.concat(rs.getString(i));
            }
        }

        return Objects.equals(wiersz, "0");
    }
}
