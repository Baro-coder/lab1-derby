package tjee.lab1.derby;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DbOkno {
    private JPanel panel_main;
    private JButton connectBt;
    private JButton printBt;
    private JButton disconnectBt;
    private JTextArea output_ta;
    private JButton insertBt;
    private boolean connected;
    private String out;

    public DbOkno() {

        connected = false;

        connectBt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    connected = DbManager.Connect();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(DbOkno.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (connected) {
                    output_ta.setText("Połączono");
                } else {
                    output_ta.setText("Nie połączono");
                }

            }
        });
        printBt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if (connected) {
                        out = DbManager.getData();
                        output_ta.removeAll();
                        output_ta.setText(out);
                    } else {
                        output_ta.setText("Nie połączono");
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(DbOkno.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });
        disconnectBt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    connected = !DbManager.Disconnect();
                } catch (SQLException ex) {
                    Logger.getLogger(DbOkno.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (!connected) {
                    output_ta.setText("Rozłączono");
                } else {
                    output_ta.setText("Nie rozłączono");
                }
            }
        });
        insertBt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(connected)
                {
                    DbInsertDialog dialog = new DbInsertDialog();
                    dialog.pack();
                    dialog.setVisible(true);

                    if (dialog.isValid)
                    {
                        String first_name = dialog.first_name;
                        String last_name = dialog.last_name;
                        try {
                            DbManager.addRecord(first_name, last_name);
                        } catch (SQLException ex) {
                            Logger.getLogger(DbOkno.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }else {
                    output_ta.setText("Nie połączono");
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("DbOkno");
        frame.setContentPane(new DbOkno().panel_main);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
